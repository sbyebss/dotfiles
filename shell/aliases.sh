#############################RC FILE############################################
if [[ $SHELL == '/bin/bash' ]]; then
    rc_file='~/.bashrc'
else
    rc_file='~/.zshrc'
fi

alias rebash="source $rc_file"
alias eb='code ~/dotfiles'
alias als='code ~/dotfiles/shell/aliases.sh'
#############################END###############################################

#############################SYSTEM############################################
alias sleep_down="systemctl suspend"
alias task='asynctask -f'
bindkey -s '\e[15~' 'task\n'
# TODO
# kill $(ps | grep 'python' | awk '{print $2}')
# rsync --filter=': -.gitignore'
#############################END###############################################

#############################GIT###############################################
alias gwd='git diff --word-diff=color'
alias gs='git status'
alias gpl='git pull'
alias gps='git push'
alias gr='git rev-parse --short HEAD'
alias gco='git checkout'
alias gc='git commit -m'
alias gb='git branch'
alias ga='git add'
alias gca='git commit --amend'
alias gm='git merge'
function diffe() { diff --width=$COLUMNS --side-by-side "$1" "$2" | less --prompt="${1//./\\.} vs ${2//./\\.}"; }
fbr() {
  local branches branch
  branches=$(git branch --all | grep -v HEAD) &&
  branch=$(echo "$branches" |
           fzf-tmux -d $(( 2 + $(wc -l <<< "$branches") )) +m) &&
  git checkout $(echo "$branch" | sed "s/.* //" | sed "s#remotes/[^/]*/##")
}

fstash() {
  local out q k sha
  while out=$(
    git stash list --pretty="%C(yellow)%h %>(14)%Cgreen%cr %C(blue)%gs" |
    fzf --ansi --no-sort --query="$q" --print-query \
        --expect=ctrl-d,ctrl-b);
  do
    mapfile -t out <<< "$out"
    q="${out[0]}"
    k="${out[1]}"
    sha="${out[-1]}"
    sha="${sha%% *}"
    [[ -z "$sha" ]] && continue
    if [[ "$k" == 'ctrl-d' ]]; then
      git diff $sha
    elif [[ "$k" == 'ctrl-b' ]]; then
      git stash branch "stash-$sha" $sha
      break;
    else
      git stash show -p $sha
    fi
  done
}

# fcs - get git commit sha
# example usage: git rebase -i `fcs`
fcs() {
  local commits commit
  commits=$(git log --color=always --pretty=oneline --abbrev-commit --reverse) &&
  commit=$(echo "$commits" | fzf --tac +s +m -e --ansi --reverse) &&
  echo -n $(echo "$commit" | sed "s/ .*//")
}

fcoc() {
  local commits commit
  commits=$(git log --pretty=oneline --abbrev-commit --reverse) &&
  commit=$(echo "$commits" | fzf --tac +s +m -e) &&
  git checkout $(echo "$commit" | sed "s/ .*//")
}
#############################END###############################################

#############################ROS###############################################
alias cw='cd ~/catkin_ws'
alias cs='cd ~/catkin_ws/src'
alias cm='cd ~/catkin_ws && catkin_make'
function ros_ ()
{
    # source /opt/ros/melodic/setup.bash
    export EDITOR='code'
    # export ROS_MASTER_URI=http://128.61.50.202:11311
    # export ROS_HOSTNAME=128.61.50.202
    source /opt/ros/melodic/setup.zsh
    source ~/catkin_ws/devel/setup.zsh
    export TURTLEBOT3_MODEL=burger
    export ROS_MASTER_URI=http://localhost:11311
    export ROS_HOSTNAME=localhost
    alias test="roslaunch Zhang_Qinsheng run.launch"
    alias riv="rviz -d `rospack find turtlebot3_navigation`/rviz/turtlebot3_navigation.rviz"
    # alias vie="roslaunch rviz -d turtlebot3_navigation/rviz/turtlebot3_navigation.rviz"
}
alias ros="ros_"

function burger_ ()
{
    export EDITOR='code'
    export ROS_MASTER_URI=http://143.215.105.38:11311
    # export ROS_MASTER_URI=http://128.61.57.60:11311
    export ROS_HOSTNAME=128.61.50.207
    source /opt/ros/melodic/setup.bash
    source ~/catkin_ws/devel/setup.bash
    export TURTLEBOT3_MODEL=burger
    alias q="rostopic pub -1 /cmd_vel geometry_msgs/Twist -- '[0, 0.0, 0.0]' '[0.0, 0.0, 0]'"
    alias test="roslaunch Zhang_Qinsheng run.launch"
    alias k="roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch"
    alias riv="rviz -d `rospack find turtlebot3_navigation`/rviz/turtlebot3_navigation.rviz"
    alias scpfinal="scp -r ~/catkin_ws/src/final burger:~/catkin_ws/src/"
    alias scpbag="scp -r burger:~/Document/rosbag/ ~/Desktop"
    alias scpbash="scp ~/Documents/cs7785/.bashrc burger:~/"

}
alias bger="burger_"
###############################END##############################################

#############################Python#############################################
alias unit="python -m unittest"
function conda_setup_ ()
{ 
    # >>> conda initialize >>>
    # !! Contents within this block are managed by 'conda init' !!
    conda_path="/home/$USER/anaconda3/bin/conda"
    __conda_setup="$($conda_path 'shell.bash' 'hook' 2> /dev/null)"
    if [ $? -eq 0 ]; then
        eval "$__conda_setup"
    else
        if [ -f "/home/$USER/anaconda3/etc/profile.d/conda.sh" ]; then
            . "/home/$USER/anaconda3/etc/profile.d/conda.sh"
        else
            export PATH="/home/$USER/anaconda3/bin:$PATH"
        fi
    fi
    unset __conda_setup
    # <<< conda initialize <<<
}
alias jjf="conda_setup_"
alias sys-dev="conda deactivate; source $rc_file"
alias cna="conda activate"
alias cda="conda deactivate"
#############################END###############################################

#############################VSCODE############################################
function vscode_update_ ()
{
    wget https://vscode-update.azurewebsites.net/latest/linux-deb-x64/stable -O /tmp/code_latest_amd64.deb
    sudo dpkg -i /tmp/code_latest_amd64.deb
}
alias vscode_update="vscode_update_"
#############################END###############################################

#############################jupyter###########################################

alias jlab="jupyter lab"

alias jlremote="jupyter lab --no-browser --port=8888"

alias jlabLocal="ssh -Y -N -L localhost:8888:localhost:8888"

#-------------------------I do not know it can work well--------------
function jllocal {
    cmd="ssh -Y -N -L localhost:8888:localhost:8888 qzhang419@gpu"
    running_cmds=$(ps aux |grep -v grep|grep "$cmd")
    if [[ "$1" == 'kill' ]]; then
        for pid in $(echo $running_cmds | awk '{print $2}'); do
            echo "kill pid $pid"
            kill -9 $pid
        done
    else
        if [ !-z $n_running_cmds ]; then
            echo "jllocal command is still running, Kill with 'jllocla kill' next time"
        else
            echo "Running cmmand '$cmd'"
            eval "$cmmd"
        fi
        url=$(ssh qhang419@gpu \
            # 'REMOTE/PATH/TO/jupyter notebook list' \
            '~' \
            |grep http | awk '{print $1}')
        echo "URL that will open in your browser:"
        echo "$url"
        open "$url"
   fi
}
#############################END###############################################

#######################Docker##################################################
alias rdk="docker -H ssh://qzhang419@128.61.184.215"
alias sdk="ssh -NL localhost:23750:/var/run/docker.sock gpu"
function dkbash {
    if [ "$#" -eq 0 ];then
        container=$(docker ps -q | awk 'FNR == 1 {print $1;}')
        docker exec -it $container bash 
    else 
        # for input a number case
        if [[ $1 == ?(-)+([0-9]) ]];then
            # it is a trick: see https://stackoverflow.com/questions/13799789/expansion-of-variable-inside-single-quotes-in-a-command-in-bash
            container=`docker ps -q | awk 'FNR == '"$1"' {print $1;} ' `
            docker ps | grep $container
            docker exec -it $container bash
        # for input string case
        else
            docker exec -it $1 bash 
        fi 
    fi
}

###############################################################################

#######################SYSTEM##################################################
fkill() {
    local pid 
    if [ "$UID" != "0" ]; then
        pid=$(ps -f -u $UID | sed 1d | fzf -m | awk '{print $2}')
    else
        pid=$(ps -ef | sed 1d | fzf -m | awk '{print $2}')
    fi  

    if [ "x$pid" != "x" ]
    then
        echo $pid | xargs kill -${1:-9}
    fi  
}

cdf() {
   local file
   local dir
   file=$(fzf +m -q "$1") && dir=$(dirname "$file") && cd "$dir"
}
# fh - repeat history
fh() {
  print -z $( ([ -n "$ZSH_NAME" ] && fc -l 1 || history) | fzf +s --tac | sed -r 's/ *[0-9]*\*? *//' | sed -r 's/\\/\\\\/g')
}

fo() {
  local out file key
  IFS=$'\n' out=("$(fzf-tmux --query="$1" --exit-0 --expect=ctrl-o,ctrl-e)")
  key=$(head -1 <<< "$out")
  file=$(head -2 <<< "$out" | tail -1)
  if [ -n "$file" ]; then
    [ "$key" = ctrl-o ] && open "$file" || ${EDITOR:-vim} "$file"
  fi
}

# f mv
f() {
    sels=( "${(@f)$(fd "${fd_default[@]}" "${@:2}"| fzf)}" )
    test -n "$sels" && print -z -- "$1 ${sels[@]:q:q}"
}
# Like f, but not recursive.
fm() f "$@" --max-depth 1

# cf - fuzzy cd from anywhere
# ex: cf word1 word2 ... (even part of a file name)
# zsh autoload function
cf() {
  local file

  file="$(locate -Ai -0 $@ | grep -z -vE '~$' | fzf --read0 -0 -1)"

  if [[ -n $file ]]
  then
     if [[ -d $file ]]
     then
        cd -- $file
     else
        cd -- ${file:h}
     fi
  fi
}