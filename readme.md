# Branch Organize

## Add zsh plugins
```shell
git submodule add https://github.com/zsh-users/zsh-autosuggestions.git zsh/plguins/zsh-autosuggestions
```

## Features
### xclip clipboard manipulation tool
- Need to transfer data between applications and terminal. Essentially, copy the data into the system clipboard. 
- Need to transfer data between ssh remote to local or local to remote. # TODO 
### git submodule
- Basically, all you need is a .gitmodules file and a empty folder corresponding to the .gitmodules, and when you clone a project, all you get, for submodule part, these two things. 
- Run `git submodule init` will register the context to the `.git/config`, when you run `git submodule update`, clone the submodule down, and record its versions and git in the `.git/modules`
## Todo 
- [ ] `Python2.7` should be addressed seperately! 
- [ ] create config folder, check if the `.local/bin` exist, do not remove same as `.config`

## Support
- [tldr](https://github.com/raylee/tldr) Easy man pages
- [fasd](https://github.com/clvv/fasd) Prezto has bundled fasd. `j` changes the current working directory. Type `,`, `f,`, `d,` in front of a comma-separated query or type `,,`,  `,,f`,`,,d` at the end of a comma-separated query then hit <kbd>tab</kbd>.
- [fd](https://github.com/sharkdp/fd) `Note` I already place the `bin` and `share` in `.local`. If need to install, run `apt install` or install from `.deb`. 
- [fzf](https://github.com/junegunn/fzf#using-the-finder) Great tools!!! `Note` Need to install through `apt`
- rsync
  - `rsync-copy` copies files and directories from *source* to *destination*.
  - `rsync-move` moves files and directories from *source* to *destination*.
  - `rsync-update` updates files and directories on *destination*.
  - `rsync-synchronize` synchronizes files and directories between *source* and
    *destination*.
- [trash-cli](https://github.com/andreafrancia/trash-cli/)
- [bat](https://github.com/sharkdp/bat#installation) A cat(1) clone with wings
## MISC
- [Install without sudo](https://askubuntu.com/a/350/740124)
- [make install specify path](https://stackoverflow.com/questions/3239343/make-install-but-not-to-default-directories/3239373)
# Thanks
The concept of global `config` and curl install in from [work](https://github.com/mrahtz/dotfiles) of mrahtz. Seems not use it.   
Now the structure is borrow from  [missing](https://github.com/anishathalye/dotfiles), directly run install can deploy directly. 
